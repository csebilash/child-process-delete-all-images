var CronJob = require('cron').CronJob;
var exec = require('child_process').exec;
var path = '../tmp/uploads';


var job = new CronJob({
    cronTime: '*/1 * * * *',
    onTick: function() {
        exec('find '+path+' -mmin +4 -type f -name "*.*" -exec rm -f {} +', function(err,stdout,stderr){
            if(err)
                console.log(err);
        });
    },
    start: false,
    timeZone: 'Asia/Dhaka'
});

job.start();
