# child-process-delete-all-images
Delete all images without directory using child process on NodeJs

# Child Process Function:
```
var exec = require('child_process').exec;
exec('find '+path+' -mmin +4 -type f -name "*.*" -exec rm -f {} +')
```
